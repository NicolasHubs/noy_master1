/*! \file pagefaultmanager.cc
Routines for the page fault managerPage Fault Manager
*/
//
//  Copyright (c) 1999-2000 INSA de Rennes.
//  All rights reserved.
//  See copyright_insa.h for copyright notice and limitation
//  of liability and disclaimer of warranty provisions.
//

#include "kernel/thread.h"
#include "vm/pagefaultmanager.h"
#include "vm/physMem.h"
#include "vm/swapManager.h"

PageFaultManager::PageFaultManager() {}

// PageFaultManager::~PageFaultManager()
/*! Nothing for now
 */
PageFaultManager::~PageFaultManager() {}

// ExceptionType PageFault(uint32_t virtualPage)
/*!
//	This method is called by the Memory Management Unit when there is a
//      page fault. This method loads the page from :
//      - read-only sections (text,rodata) $\Rightarrow$ executive
//        file
//      - read/write sections (data,...) $\Rightarrow$ executive
//        file (1st time only), or swap file
//      - anonymous mappings (stack/bss) $\Rightarrow$ new
//        page from the MemoryManager (1st time only), or swap file
//
//	\param virtualPage the virtual page subject to the page fault
//	  (supposed to be between 0 and the
//        size of the address space, and supposed to correspond to a
//        page mapped to something [code/data/bss/...])
//	\return the exception (generally the NO_EXCEPTION constant)
*/
ExceptionType PageFaultManager::PageFault(uint32_t virtualPage) {
#ifndef ETUDIANTS_TP
  printf("**** Warning: method PageFaultManager::PageFault is not implemented "
         "yet\n");
  exit(-1);
#endif
#ifdef ETUDIANTS_TP
  Process *proc = g_current_thread->GetProcessOwner();
  AddrSpace *addrSpace = proc->addrspace;
  TranslationTable *translationtable = addrSpace->translationTable;
  OpenFile *mappedFile;

  int adrDisk = translationtable->getAddrDisk(virtualPage);
  bool bitSwap = translationtable->getBitSwap(virtualPage);
  char pageTmp[g_cfg->PageSize];

  while (translationtable->getBitIo(virtualPage)) {
    g_current_thread->Yield();
  }
  translationtable->setBitIo(virtualPage);

  if (bitSwap == 1) {
    while (adrDisk == -1) {
      // attendre car il y a déjà un voleur de page qui met notre page dans le
      // swap.
      g_current_thread->Yield();
    }
    g_swap_manager->GetPageSwap(translationtable->getAddrDisk(virtualPage),
                                pageTmp);
    g_swap_manager->ReleasePageSwap(translationtable->getAddrDisk(virtualPage));
    translationtable->clearBitSwap(virtualPage);
  } else {
    if (adrDisk == -1) {
      // Page anonyme
      memset(pageTmp, 0, g_cfg->PageSize);
    } else if ((mappedFile = addrSpace->findMappedFile(
                    virtualPage * g_cfg->PageSize)) != NULL) {
      // Fichier mappé
      mappedFile->ReadAt(pageTmp, g_cfg->PageSize, adrDisk);
    } else {
      proc->exec_file->ReadAt(pageTmp, g_cfg->PageSize, adrDisk);
    }
  }

  // demande au gestionnaire de mémoire réelle de donner une page réelle libre
  int addrPhys = g_physical_mem_manager->AddPhysicalToVirtualMapping(
      addrSpace, virtualPage);
  memcpy(&(g_machine->mainMemory[addrPhys * g_cfg->PageSize]), pageTmp,
         g_cfg->PageSize);
  translationtable->setPhysicalPage(virtualPage, addrPhys);
  translationtable->setBitValid(virtualPage);
  g_physical_mem_manager->UnlockPage(addrPhys);
  translationtable->clearBitIo(virtualPage);

  return NO_EXCEPTION;
#endif
}

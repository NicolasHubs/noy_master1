/* \file drvACIA.cc
   \brief Routines of the ACIA device driver
//
//      The ACIA is an asynchronous device (requests return
//      immediately, and an interrupt happens later on).
//      This is a layer on top of the ACIA.
//      Two working modes are to be implemented in assignment 2:
//      a Busy Waiting mode and an Interrupt mode. The Busy Waiting
//      mode implements a synchronous IO whereas IOs are asynchronous
//      IOs are implemented in the Interrupt mode (see the Nachos
//      roadmap for further details).
//
//  Copyright (c) 1999-2000 INSA de Rennes.
//  All rights reserved.
//  See copyright_insa.h for copyright notice and limitation
//  of liability and disclaimer of warranty provisions.
//
*/

/* Includes */

#include "drivers/drvACIA.h"
#include "kernel/synch.h"
#include "kernel/system.h" // for the ACIA object
#include "machine/ACIA.h"

//-------------------------------------------------------------------------
// DriverACIA::DriverACIA()
/*! Constructor.
  Initialize the ACIA driver. In the ACIA Interrupt mode,
  initialize the reception index and semaphores and allow
  reception and emission interrupts.
  In the ACIA Busy Waiting mode, simply inittialize the ACIA
  working mode and create the semaphore.
  */
//-------------------------------------------------------------------------

DriverACIA::DriverACIA() {
#ifndef ETUDIANTS_TP
  printf("**** Warning: constructor of the ACIA driver not implemented yet\n");
  exit(-1);
#endif
#ifdef ETUDIANTS_TP
  ind_send = 0;
  ind_rec = 0;

  char *sendBuffName = (char *)"sema_Send";
  char *receiveBuffName = (char *)"sema_Receive";

  bzero(send_buffer, BUFFER_SIZE);
  bzero(receive_buffer, BUFFER_SIZE);

  send_sema = new Semaphore(sendBuffName, 1);
  if (g_cfg->ACIA == ACIA_INTERRUPT) {
    receive_sema = new Semaphore(receiveBuffName, 0);
    g_machine->acia->SetWorkingMode(SEND_INTERRUPT | REC_INTERRUPT);
  } else if (g_cfg->ACIA == ACIA_BUSY_WAITING) {
    receive_sema = new Semaphore(receiveBuffName, 1);
    g_machine->acia->SetWorkingMode(BUSY_WAITING);
  }
#endif
}

//-------------------------------------------------------------------------
// DriverACIA::TtySend(char* buff) dric
/*! Routine to send a message through the ACIA (Busy Waiting or Interrupt mode)
 */
//-------------------------------------------------------------------------

int DriverACIA::TtySend(char *buff) {
#ifndef ETUDIANTS_TP
  printf(
      "**** Warning: method Tty_Send of the ACIA driver not implemented yet\n");
  exit(-1);
#endif
#ifdef ETUDIANTS_TP
  //incrémentation du sémaphore d'envoie
  send_sema->P();
  if (send_sema<0) printf("Je suis en attente du relachement du sémaphore\n");
  ind_send = 0;
  int cpt = 0;

  // Evite d'avoir une erreur dans la boucle
  // si on avait envoyé '\0' precedemment
  bzero(send_buffer, BUFFER_SIZE);

  // Copie de la chaîne dans le tampon d'émission si ce dernier est disponible
  memcpy(send_buffer, buff, BUFFER_SIZE);

  if (g_cfg->ACIA == ACIA_BUSY_WAITING) {
    // Envoie des messages par attente active
    do {
      //Attente active. Permet d'attendre jusqu'à ce que le registre OutputStateRegister passe à EMPTY.
      //On ne peut écrire dans l'outputRegister si notre caractère précedant n'a pas encore été à l'inputRegister.
      while (g_machine->acia->GetOutputStateReg() != EMPTY)
        ;

      printf("(send) char : %c\n", send_buffer[ind_send]);
      //Ecriture du caractère dans le registre de données en émission
      g_machine->acia->PutChar(send_buffer[ind_send]);
      ind_send++;
    } while ((send_buffer[ind_send - 1] != '\0'));
    printf("Taille du message envoyé: %lu\n", strlen(send_buffer));
    printf("Message envoyé: %s\n\n\n", send_buffer);
    send_sema->V();

  } else if (g_cfg->ACIA == ACIA_INTERRUPT) {
    // Envoi du premier caractère pour lancer la routine d'interruption de reception
    g_machine->acia->PutChar(send_buffer[ind_send]);
    printf("J'envoie le caractère --> %c\n", send_buffer[ind_send]);
    ind_send++;
  }
  return strlen(send_buffer);
#endif
}

//-------------------------------------------------------------------------
// DriverACIA::TtyReceive(char* buff,int length)
/*! Routine to reveive a message through the ACIA
//  (Busy Waiting and Interrupt mode).
  */
//-------------------------------------------------------------------------

int DriverACIA::TtyReceive(char *buff, int lg) {
#ifndef ETUDIANTS_TP
  printf("**** Warning: method Tty_Receive of the ACIA driver not implemented "
         "yet\n");
  exit(-1);
  return 0;
#endif
#ifdef ETUDIANTS_TP
  if (lg <= 0)
    return -1;

  //incrémentation du sémaphore de réception
  //Bloque le thead appelant dans le cas des interruptions
  receive_sema->P();
  ind_rec = 0;
  int cpt = 0;

  if (g_cfg->ACIA == ACIA_BUSY_WAITING) {
      // Evite d'avoir une erreur dans la boucle
      // si on avait envoyé '\0' precedemment
    bzero(receive_buffer, BUFFER_SIZE);
    do {
      //Attente active. Permet d'attendre jusqu'à ce que le registre inputStateRegister passe à FULL
      while (g_machine->acia->GetInputStateReg() == EMPTY)
        ;
      //Ecriture du caractère dans le registre de données en émission
      receive_buffer[ind_rec] = g_machine->acia->GetChar();
      ind_rec++;
      printf("(Receive) char : %c\n", receive_buffer[ind_rec - 1]);
    } while ((receive_buffer[ind_rec - 1] != '\0') && (ind_rec < lg));
    memcpy(buff, receive_buffer, lg);
    printf("Taille du message reçu: %lu\n", strlen(buff));
    printf("Message reçu: %s\n\n\n", buff);
    receive_sema->V();

  } else if (g_cfg->ACIA == ACIA_INTERRUPT) {
    //copie le contenu du receive_buffer dans la chaîne buff
    memcpy(buff, receive_buffer, lg);
    g_machine->acia->SetWorkingMode(SEND_INTERRUPT | REC_INTERRUPT);
    bzero(receive_buffer, BUFFER_SIZE);
  }
  return strlen(buff);
#endif
}

//-------------------------------------------------------------------------
// DriverACIA::InterruptSend()
/*! Emission interrupt handler.
  Used in the ACIA Interrupt mode only.
  Detects when it's the end of the message (if so, releases the send_sema
  semaphore), else sends the next character according to index ind_send.
  */
//-------------------------------------------------------------------------

void DriverACIA::InterruptSend() {
#ifndef ETUDIANTS_TP
  printf("**** Warning: send interrupt handler not implemented yet\n");
  exit(-1);
#endif
#ifdef ETUDIANTS_TP
  // Vérifie s'il on a déjà envoyer ou non le caractère de fin de chaîne
  if (send_buffer[ind_send - 1] == '\0') {
    printf("J'ai fini d'envoyer\n");
    printf("Taille du message envoyé: %lu\n", strlen(send_buffer));
    printf("Message envoyé: %s\n\n\n", send_buffer);

    // On relâche le sémaphore
    send_sema->V();
  } else {
    // On continue d'envoyer notre chaîne
    g_machine->acia->PutChar(send_buffer[ind_send]);
    printf("J'envoie le caractère --> %c\n", send_buffer[ind_send]);
    ind_send++;
  }
#endif
}

//-------------------------------------------------------------------------
// DriverACIA::Interrupt_receive()
/*! Reception interrupt handler.
  Used in the ACIA Interrupt mode only. Reveices a character through the ACIA.
  Releases the receive_sema semaphore and disables reception
  interrupts when the last character of the message is received
  (character '\0').
  */
//-------------------------------------------------------------------------

void DriverACIA::InterruptReceive() {
#ifndef ETUDIANTS_TP
  printf("**** Warning: receive interrupt handler not implemented yet\n");
  exit(-1);
#endif
#ifdef ETUDIANTS_TP
  char c = g_machine->acia->GetChar();
  // Vérifie s'il on a déjà envoyer ou non le caractère de fin de chaîne ou si notre tampon de réception va dépasser la taille maximal
  if ((ind_rec == (BUFFER_SIZE - 1)) || (c == '\0')) {
    //Ecrire le caractère de fin de chaine dans le tampon de réception
    receive_buffer[ind_rec] = '\0';
    printf("Je reçois le caractère --> %c\n", receive_buffer[ind_rec]);
    printf("Taille du message reçu --> %lu\n", strlen(receive_buffer));
    printf("Message reçu --> %s\n\n\n", receive_buffer);
    //g_machine->acia->SetWorkingMode(g_machine->acia->GetWorkingMode()^REC_INTERRUPT);
    // On relâche le sémaphore --> Cela va débloquer le thread courant et lui permettre
    // d'écrire le contenu du receive_buffer dans la chaîne buff.
    receive_sema->V();
  } else {
    //Ecrire le caractère dans le tampon de réception
    receive_buffer[ind_rec] = c;
    printf("Je reçois le caractère --> %c\n", receive_buffer[ind_rec]);
    ind_rec++;
  }
#endif
}

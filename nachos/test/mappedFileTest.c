// Nachos system calls
#include "userlib/libnachos.h"

#define TAB_SIZE 17

void bubbleSort(char *array, int n) {
  int i, j, temp, flag;

  n_printf("\nTri en cours\n");
  for (i = 0; i < n; i++) {
    flag = 0;
    for (j = 0; j < n - i - 1; j++) {
      if (array[j] > array[j + 1]) {
        flag = 1;
        temp = array[j + 1];
        array[j + 1] = array[j];
        array[j] = temp;
      }
    }
    if (flag==0) {
      return;
    }
  }
}

int main() {
  OpenFileId fileId;
  char *tabToSort;
  int createFile = Create("sortTest", TAB_SIZE);
  if (createFile != 0) {
    n_printf("Erreur pendant la création du fichier.\n");
    return -1;
  }
  n_printf("Fichier créé.\n");
  fileId = Open("sortTest");
  n_printf("Fichier ouvert.\n");
  char array[TAB_SIZE] = {'8', '7', '6', '3', '6', '9', '7', '1',
                          '5', '2', '1', '4', '5', '9', '5', '3' ,'\0'};

  Write(array, TAB_SIZE, fileId);

  tabToSort = (char *)Mmap(fileId, TAB_SIZE);
  if ((int)*tabToSort == -1) {
    n_printf("Erreur pendant le mapping du fichier.\n");
    return -1;
  }

  n_printf(tabToSort);
  bubbleSort(tabToSort, TAB_SIZE - 1);
  n_printf(tabToSort);

  Close(fileId);
  n_printf("\nFichier fermé.\n");

  return 0;
}

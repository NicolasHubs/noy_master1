/* receiveINTERRUPT.c
 */

#include "userlib/syscall.h"
#include "userlib/libnachos.h"

#define BUFFER_SIZE 256

void fonction() {
    char message[BUFFER_SIZE];
    TtyReceive(message, BUFFER_SIZE);
    n_printf("Message reçu: %s\n\n\n", message);
    Halt();
}

int main()
{
    n_printf("Je suis le receiver\n");
    ThreadId thread1 = threadCreate("thread1", &fonction);
    return 0;
}

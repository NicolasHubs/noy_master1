/* sendBUSY.c
*/

#include "userlib/syscall.h"
#include "userlib/libnachos.h"

void fonction1() {
    char* message = "TestMessage1\0";
    TtySend(message);
}

void fonction2() {
    char* message2 = "TestMessage2\0";
    TtySend(message2);
    Halt();
}

int main()
{
    n_printf("Je suis le sender\n");
    ThreadId thread1 = threadCreate("thread1", &fonction1);
    ThreadId thread2 = threadCreate("thread2", &fonction2);
    return 0;
}

#include "userlib/syscall.h"
#include "userlib/libnachos.h"

SemId sem1;
SemId sem2;

void fonction1() {
	n_printf("** P(sem2) met le thread 1 en attente **\n");
	P(sem2);
	n_printf("** En attente du sémaphore **\n");
	V(sem1);
	int tmp = SemDestroy(sem1);
	int tmp2 = SemDestroy(sem2);
	n_printf("** Fin du thread1 **%d et %d\n", tmp, tmp2);
}

void fonction2() {
	n_printf("** V(sem2) incrémente le sem 2 et donc libère le thread 1 **\n");
	V(sem2);
	n_printf("** P(sem1) met le thread 2 en attente **\n");
	P(sem1);
	int tmp = SemDestroy(sem1);
	int tmp2 = SemDestroy(sem2);
	n_printf("** Fin du thread 2 **%d et %d\n\n\n", tmp, tmp2);
}

int main() {
	sem1 = SemCreate("sem1", 0);
	sem2 = SemCreate("sem2", 0);
	n_printf("** ** ** Nachos ** ** **\n");
	ThreadId thread1 = threadCreate("thread1", &fonction1);
	ThreadId thread2 = threadCreate("thread2", &fonction2);
 	return 0;
}

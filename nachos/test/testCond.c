#include "userlib/syscall.h"
#include "userlib/libnachos.h"

#define SIZE 10

CondId cond;

void fonction1() {
        int i = 1;
        while(i <= 10) {
                n_printf("\nJe suis le thread1 et je suis dans ma boucle n°%d\n",i);
                i++;
        }
        n_printf("--> Le thread1 libère le thread2 bloqué sur la condition cond\n");
        CondSignal(cond);
}

void fonction2() {
        int j = 1;
        while(j <= 10) {
                if(j==5){
                  n_printf("--> Met dans l’ensemble d’attente associée de la condition cond le thread 2\n");
                  CondWait(cond);
                }
                n_printf("\nJe suis le thread2 et je suis dans ma boucle n°%d\n",j);
                j++;
        }
        n_printf("\n\n\n");
}

int main() {
        cond = CondCreate("Cond");
        n_printf("** ** ** Nachos ** ** **\n");
        ThreadId thread1 = threadCreate("thread1", &fonction1);
        ThreadId thread2 = threadCreate("thread2", &fonction2);
        return 0;
}

#include "userlib/syscall.h"
#include "userlib/libnachos.h"

#define SIZE 10

LockId verrou;

void fonction1() {
        int i = 0;
        LockAcquire(verrou);
        while(i < 11) {
                n_printf("i = %d\n",i);
                i++;
        }
        LockRelease(verrou);
}

void fonction2() {
        int j = 11;
        LockAcquire(verrou);
        while(j > 0) {
                n_printf("j = %d\n",j);
                j--;
        }
        LockRelease(verrou);
}

int main() {
        verrou = LockCreate("Lock");
        n_printf("** ** ** Nachos ** ** **\n");
        ThreadId thread1 = threadCreate("thread1", &fonction1);
        ThreadId thread2 = threadCreate("thread2", &fonction2);
        return 0;
}
